import React, { Component } from "react";
import { navigate, Redirect } from "@reach/router";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CustomCard from '../components/CustomCard'
import TabelaDespesa from '../components/TabelaDespesa'
import FloatingButton from '../components/FloatingButton'
import * as Actions from '../actions/despesa';




class ListaDespesas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      despesas: []
    }
    this.getDespesas = _ => {
      this.props.GetDespesas(12);
    }

    this.fabAction = _ => {
      navigate("/editaDespesas")
    }

  }

  componentDidMount() {
    this.getDespesas()
  }

  render() {
    const usuarioLogado = this.props.Usuario.get('logado');
    if (usuarioLogado) {
      return (
        <div>
          <CustomCard>
            <TabelaDespesa despesas={this.props.Despesas.get('despesas').toJS()} />
          </CustomCard>
          <FloatingButton action={this.fabAction} />
        </div>
      )
    } else {

      return (
      <div>
        <p>foi mal</p>
        <Redirect to="/" noThrow/>
      </div>)
    }
  }
}

const mapStateToProps = (state) => {
  return {
    Usuario: state.Usuario,
    Despesas: state.Despesas
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListaDespesas);