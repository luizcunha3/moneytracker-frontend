import React, { Component } from "react";
import TextField from "@material-ui/core/TextField"
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { navigate } from '@reach/router'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Typography } from '../../node_modules/@material-ui/core';
import CenteredCard from '../components/CenteredCard'
import * as Actions from '../actions/usuario';


class LoginCard extends Component {
  constructor(props) {
    super(props);

    this.state = { email: "", senha: "" }

    this.handleChange = name => event => this.setState({ [name]: event.target.value })

    this.handleLogin = _ => {
      // firebaseAuth.signInWithEmailAndPassword(this.state.email, this.state.senha)
      //   .then(user => console.log(user))
      //   .catch(err => {
      //     console.log(err)
      //   })
    }


    this.styles = {
      textField: {
        marginTop: '20px!important'
      }
    }

  }

  componentDidUpdate() {
    this.props.Usuario.get('logado') ? navigate('/lista') : null
  }
  
  render() {
    const { classes } = this.props
    return (
      <CenteredCard>
        <Typography variant="headline">Money Tracker</Typography>
        <Typography variant="subheading">Bem vindo ao Money Tracker</Typography>
        <div>
          <TextField
            id="email"
            label="Email"
            value={this.state.email}
            className={classes.textField}
            onChange={this.handleChange('email')}
          /><br />
          <TextField
            id="senha"
            label="Senha"
            value={this.state.senha}
            className={this.styles.textField}
            onChange={this.handleChange('senha')}
            type="password"
          /><br />
          <Button variant="contained" color="primary" onClick={this.handleLogin}>Login</Button>
          <Button variant="contained">Sign in</Button> <br />
          <Button variant="contained" color="secondary" onClick={this.props.HandleGoogle}>Google</Button>
        </div>
      </CenteredCard>
    )
  }
}

LoginCard.propTypes = {
  classes: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => {
  return {
    Usuario: state.Usuario
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(Actions, dispatch);
}

export default withStyles(this.styles)(connect(mapStateToProps, mapDispatchToProps)(LoginCard)); 