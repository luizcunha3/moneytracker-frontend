import React, { Component } from 'react';
import { Router, Link } from "@reach/router"

import ListaDespesas from './pages/ListaDespesas'
import EditaDespesas from './pages/EditaDespesa'
import ApplicationBar from './components/ApplicationBar';
import LoginCard from './pages/LoginCard'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      usuario: {
        fake: true
      }
    }
    this.setUsuario = user => {
      this.setState({usuario: user})
    }
  }
  render() {
    return (
      <div>
        <ApplicationBar />
        <Router>
          <LoginCard setUsuario={this.setUsuario} path="/" />
          <ListaDespesas usuario={this.state.usuario} path="/lista" />
          <EditaDespesas usuario={this.state.usuario} path="/editaDespesas" />
        </Router>
      </div>
    );
  }
}

export default App;
