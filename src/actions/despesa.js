import axios from 'axios';

import config from '../utils/conf';

export const GET_DESPESAS = 'GET_DESPESAS';
export const GET_DESPESAS_ERRO = 'GET_DESPESAS_ERRO';

export const GetDespesas = idUsuario =>{
  return dispatch => {
    axios.get(`${config.moneyTrackerBackend}${config.despesas}`)
      .then(response => dispatch({type: GET_DESPESAS, payload: response.data}))
      .catch(err => console.error(err))
  }
}
