import axios from 'axios';

import { firebaseAuth, firebaseGoogleAuth } from '../utils/firebaseUtils';
import config from '../utils/conf';

export const AUTH_USER = 'AUTH_USER';
export const AUTH_USER_ERROR = 'AUTH_USER_ERROR';
export const CHECK_USER = 'CHECK_USER';
export const CHECK_USER_ERROR = 'CHECK_USER_ERROR';
export const USER_LOGOUT = 'USER_LOGOUT';

export const HandleGoogle = _ => {
  return dispatch => {
    firebaseAuth.signInWithPopup(firebaseGoogleAuth)
      .then(resultGoogleAuth => {
        axios.post(`${config.moneyTrackerBackend}${config.login}`, resultGoogleAuth, { ContentType: 'application/json', AccessControlAllowOrigin: "*" })
          .then(result => {
            dispatch({ type: AUTH_USER, payload: result.data.usuario })
            // this.props.setUsuario()
            // navigate("/lista")
          })
          .catch(err => console.error(err))
      })
      .catch(err => {
        console.error(err)
      })
  }
}

export const HandleLogout = _ => {
  return dispatch => {
    dispatch({ type: USER_LOGOUT })
  }
}