import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'

const styles = {
  
    button: {
      position: 'fixed',
      bottom: 0,//theme.spacing.unit * 2,
      right: 0,//theme.spacing.margin * 2
      marginRight: '1.5rem',
      marginBottom: '1.5rem'
    }
}

class FloatingButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button variant="fab" color="primary" className={classes.button} onClick={this.props.action}>
          <AddIcon />
        </Button>
      </div>
    )
  }
}

FloatingButton.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(FloatingButton);