import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

const styles = {
  card: {
    width: '50%',
    height: '50%',
    margin: 'auto',
    position: 'absolute',
    top: '0', left: '0', bottom: '0', right: '0',
    textAlign: 'center'
  }
}
class CenteredCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>{this.props.children}</CardContent>
      </Card>
    )
  }
}

CenteredCard.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CenteredCard);