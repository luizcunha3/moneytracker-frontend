import React, { Component } from "react";
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import Edit from '@material-ui/icons/Edit'
import swal from 'sweetalert2';
import { navigate } from '@reach/router'

const styles = {
  table: {
    minWidth: 700
  }
}

class TabelaDespesa extends Component {
  constructor(props) {
    super(props);
    this.populateBody = despesas => {
      return despesas.map(despesa => {
        return (
          <TableRow>
            <TableCell>{despesa.descricao}</TableCell>
            <TableCell>{despesa.valor}</TableCell>
            <TableCell>{despesa.data}</TableCell>
          </TableRow>
        )
      })
    }
  }

  render() {
    const { classes } = this.props
    return (
      <div>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Descrição</TableCell>
              <TableCell>Valor</TableCell>
              <TableCell>Data</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.populateBody(this.props.despesas)}
          </TableBody>
        </Table>
      </div>
    )
  }
}
TabelaDespesa.propTypes = {
  classes: PropTypes.object.isRequired
}
export default withStyles(styles)(TabelaDespesa);