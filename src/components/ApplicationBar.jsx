import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as Actions from '../actions/usuario';

const styles = {
  flex: {
    flexGrow: 1
  }
}

class ApplicationBar extends Component {
  constructor(props) {
    super(props);
    this.getNomeUsuario = _ => {
      return this.props.Usuario.get('usuario').get('displayName').split(' ')[0]
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography variant="title" color="inherit" className={classes.flex}>Money Tracker</Typography>
          <Button color="inherit">{this.getNomeUsuario()}</Button>
          <Button color="inherit" onClick={this.props.HandleLogout}>Logout</Button>
        </Toolbar>
      </AppBar>
    )
  }
}

ApplicationBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => {
  return {
    Usuario: state.Usuario
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(Actions, dispatch)
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ApplicationBar));
