import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

const styles = {
  card: {
    width: '90%',
    margin: '0 auto'
  }
}
class CustomCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>{this.props.children}</CardContent>
      </Card>
    )
  }
}

CustomCard.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(CustomCard);