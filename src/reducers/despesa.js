import { fromJS } from 'immutable'
import {GET_DESPESAS,GET_DESPESAS_ERRO} from '../actions/despesa';

const initialState = fromJS({
  despesas: [],
  loading: true
})

const despesaReducer = (state = initialState, action) => {
  switch(action.type){
    case GET_DESPESAS:
      return state.mergeDeep({despesas: action.payload, loading: false})
    case GET_DESPESAS_ERRO:
      return state;
    default:
      return state;
  }
}

export default despesaReducer;