import { combineReducers } from 'redux';
import Usuario from './usuario';
import Despesas from './despesa';

const reducer = combineReducers({Usuario, Despesas})

export default reducer;