import { fromJS } from 'immutable';
import { AUTH_USER, AUTH_USER_ERROR, CHECK_USER, CHECK_USER_ERROR, USER_LOGOUT } from '../actions/usuario';

const initialState = fromJS({
  usuario: {
    email: '',
    displayName: '',
    uid: '',
    photoURL: '',
    idUsuario: '',
    usuarioNovo: false
  },
  logado: false
})

const usuarioReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_USER:
      return state.mergeDeep({ usuario: action.payload, logado: true })

    case AUTH_USER_ERROR:
      return state;

    case CHECK_USER:
      return state;

    case CHECK_USER_ERROR:
      return state;

    case USER_LOGOUT:
      return initialState

    default:
      return state;

  }
}

export default usuarioReducer;
