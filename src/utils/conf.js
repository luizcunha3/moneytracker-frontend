import prodEnv from './envs/production';
import devEnv from './envs/development';

const env = process.env.NODE_ENV 
const config = env => {
  console.log(env, '@luiz')
  switch (env) {
    case 'development':
      return prodEnv;
    case 'production':
      return prodEnv;
    default:
      return prodEnv;
  }
}
const environment = config(env)
export default environment;