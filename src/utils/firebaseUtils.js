import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDDMoVytBnmT-TK7O_GVhxihrWHHBDsLYk",
  authDomain: "moneytracker-ea12d.firebaseapp.com",
  databaseURL: "https://moneytracker-ea12d.firebaseio.com",
  projectId: "moneytracker-ea12d",
  storageBucket: "moneytracker-ea12d.appspot.com",
  messagingSenderId: "124040212029"
}

export const firebaseImpl = firebase.initializeApp(config);
export const firebaseDatabase = firebase.database();
export const firebaseAuth = firebase.auth();
export const firebaseGoogleAuth = new firebase.auth.GoogleAuthProvider();